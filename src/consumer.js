import "./style.css"

let Queue = require('promise-queue');

const lookAddr = 'GAAJGBMOYYO7CCJDYR7O53ZLDTLEJBOSYEB4EMSZCSBBB45KLGL73ORQ';

const productsAddr = 'GDA4CN76BLEBX2R6DNL47GXXJHMHTYNIECUBEIISL2BA34K4CBDXDDKW';
const wasteAddr = 'GAQJLX2K7ZSTXYDAZBL3D5EDZGPNT6STMUAA5PVAKPAY73LAHGDMFRRX';
// priv SDMDEFQ4HWQ7KSEEZYKCLVB5ZZ2YBOU7IFOPBK4DQHTVXUXANT7HGK4E
// waste SBAROHAPYJ3XHGDRZJ3SSCZTQ2XNMQQT7KOFY4HXL2AVDT4LVXPLUC4M

const ingredient = 'Ingrediente';
const additives = 'Spezie/Aromi/Additivi';
const vegetables = 'Verdure/Ortaggi/Creme';
const others = 'Salumi/Formaggi/Altro';
const packaging = 'Art. Confezionamento';
const keyword = 'bomb';


window.consumer = {
	stopSearch: false,

	safe: true,
	products: [],
	productsQueue: null,

	consumedProducts: [],
	consumedProductsQueue: null,

	actionQueue: null,

	start: function() {
		this.actionQueue = new Queue(1);
		this.productsQueue = new Queue();
		this.consumedProductsQueue = new Queue();

		this.trace();
	},

	trace: function() {
		ledger.getTxs(productsAddr)
		.then(page => {
			console.log(page);
			if (page.records.length > 0) {
				consumer.onTxRecords(this.products, this.productsQueue, page,
					[ledger.filterInputTxs,
					ledger.filterDataTxs,
					consumer.transformMemos]);
			}
		});
	},

	onTxRecords: function(array, promiseQueue, prevPage, criteria) {
		if (prevPage.records.length > 0) {
			promiseQueue.add(function() { return prevPage.next(); })
			.then(page => consumer.onTxRecords(array, promiseQueue, page, criteria));

			let txs = prevPage.records;
			if (Array.isArray(txs)) {
				if (typeof(criteria[0]) == 'function') {
					for (let i = 0; i < criteria.length; i++) {
						console.log(criteria[i]);
						txs = criteria[i](txs);
					}
				}
			}
		}
	},

	transformMemos: function(txs) {
		let getHex = function(tx) {
			return new Promise(function(resolve) {
				resolve(utils.fromBase64toHex(tx.memo));
			})
		}

		let getMultiHash = function(hexStr) {
			return new Promise(function(resolve) {
				resolve(utils.fromHexToBase58(hexStr));
			});
		}

		Promise.all(txs.map(getHex))
		.then(hexStrs => Promise.all(hexStrs.map(getMultiHash)))
		.then(multiHashes => storage.getChunks(multiHashes,
			consumer.viewFile,
			txs));
	},

	viewFile: function(txs, files) {
		for (let i = 0; i < files.length; i++) {
			if (!consumer.stopSearch) {
				let product = JSON.parse(files[i]);
				if (product.product.toLowerCase().indexOf(keyword) !== -1) {
					consumer.stopSearch = true;

					let info = document.getElementById('bc-info');
					info.innerHTML = JSON.stringify(product);
				}
			}
		}
	},

	displayProducts: function(txs, files) {
		files.forEach(function(file, index) {
			let product = JSON.parse(file);
			if (product.type === ingredient
				|| product.type === undefined)
			{
				let table = document.getElementById('availableComponentsTable');
				let row = table.insertRow(-1);

				let hiddenCell = row.insertCell(-1);
				console.log(txs[index].memo);
				hiddenCell.innerHTML = txs[index].memo;
				hiddenCell.hidden = true;

				hiddenCell = row.insertCell(-1);
				// hiddenCell.innerHTML = multiHashes[index];
				hiddenCell.innerHTML = txs[index].id;
				hiddenCell.hidden = true;

				let input = document.createElement('input');
				input.type = 'button';
				input.value = '+';
				input.style = 'width: 100%;';
				input.onclick = function() {
					lot.moveToIngredient(row);
					row.hidden = true;
				}
				row.insertCell(-1).appendChild(input);

				let cell = row.insertCell(-1);
				cell.innerHTML = product.item;
				cell.align = 'center';
				row.insertCell(-1).innerHTML = product.product;

				cell = row.insertCell(-1);filter
				cell.innerHTML = product.lot;
				cell.align = 'center';
				// row.insertCell(-1).innerHTML search string case insensitive= product.quantity;

				input = document.createElement('input');
				input.type = 'button';
				input.value = 'Scarta';
				input.style = 'width: 100%;';
				input.onclick = function() {
					lot.killProduct(row.rowIndex);
				};

				row.insertCell(-1).appendChild(input);
			}
			else {
				if (product.type == additives) {

				}
				else if (product.type == vegetables) {

				}
				else if (product.type == others) {

				}
				else if (product.type == packaging) {
					let table = document.getElementById('packaging');
					let row = table.insertRow(filter-1);

					let cell = row.insertCell(-1);
					cell.hidden = true;
					cell.innerHTML = txs[index].memo;

					cell = row.insertCell(-1);
					cell.hidden = true;
					cell.innerHTML = file;

					cell = row.insertCell(-1);
					let input = document.createElement('input');
					input.type = 'button';
					input.value = '+';
					input.style = 'width: 100%;';
					input.onclick = function() {
						lot.moveAux(row, product, txs[index]);
						row.hidden = true;
					}
					cell.appendChild(input);
					let small = document.createElement('small');
					row.insertCell(-1).appendChild(small).innerHTML = product.product;
					small = document.createElement('small');
					row.insertCell(-1).appendChild(small).innerHTML = product.lot;

					input = document.createElement('input');
					input.type = 'button';
					input.value = '-';
					input.style = 'width: 100%;';
					input.onclick = function() {
						lot.killAuxProduct(row.rowIndex, table);
					};
					row.insertCell(-1).appendChild(input);
				}
				else {
					console.log('alt area');
				}
			}
		});
	},

	getTransactions: function() {
		let self = this;

		ledger.getTxs(wasteAddr)
		.then(page => {
			// on waste txs select only the ones incoming from main account
			self.consumedProducts.push(...ledger.filterOutputTxs(page.records));
			console.log(ledger.filterOutputTxs(page.records));
			return page.next();
		})
		.then(page => {
			self.consumedProducts.push(...ledger.filterOutputTxs(page.records));
			return page.next();
		})
		.then(page => {
			if (page.records.length > 0) {
				self.onTxRecords(self.consumedProducts, self.consumedProductsQueue,
					page, [ledger.filterOutputTxs, ledger.filterDataTxs]);
			}

			ledger.getTxs('GAAJGBMOYYO7CCJDYR7O53ZLDTLEJBOSYEB4EMSZCSBBB45KLGL73ORQ')
			.then(page => {
				if (page.records.length > 0) {
					self.onTxRecords(self.products, self.productsQueue, page,
						[ledger.filterInputTxs,
						ledger.filterDataTxs,
						self.getAvailableProducts,
						self.transformMemos]);
				}
			});
		});
	},

	addProduct: function() {
		let table = document.getElementById('productsTable');
		let row = table.insertRow(-1);

		let cell = row.insertCell(-1);
		let input = document.createElement('input');
		input.type = 'text';
		input.style = 'width: 100%;';
		cell.appendChild(input);

		cell = row.insertCell(-1);
		input = document.createElement('input');
		input.type = 'text';
		input.style = 'width: 100%;';
		cell.appendChild(input);

		cell = row.insertCell(-1);
		input = document.createElement('input');
		input.type = 'number';
		input.style = 'width: 100%;';
		cell.appendChild(input);

		cell = row.insertCell(-1);
		input = document.createElement('input');
		input.type = 'button';
		input.style = 'width: 100%;';
		input.value = 'Elimina';
		input.onclick = function() {
			table.deleteRow(row.rowIndex);
		}
		cell.appendChild(input);
	},

	submitLot: function() {
		let productsTable = document.getElementById('productsTable');
		let componentsTable = document.getElementById('componentsTable');

		let productsHeader = Array.from(productsTable.rows[0].cells)
		.map(cell => { return cell.innerHTML; });
		productsHeader.splice(productsHeader.length - 1, 1);
		const products = Array.from(productsTable.rows).slice(1);

		let componentsHeader = Array.from(componentsTable.rows[0].cells)
		.map(cell => { return cell.innerHTML; });
		componentsHeader.splice(componentsHeader.length - 1, 1);
		const components = Array.from(componentsTable.rows).slice(1);

		if (products.length > 0) {
			let lot = {};
			lot['count'] = document.getElementById('idDoc').value;
			lot['lot'] = document.getElementById('idLot').value;
			lot['beaten'] = document.getElementById('radioAbb').checked;

			let files = products.map(product => {
				let file = {};
				// productsHeader.forEach(function(key, index) {
				// 	file[key] = product.cells[index].children[0].value;
				// })
				file['productLot'] = lot.lot;
				file['productCode'] = product.cells[0].children[0].value;
				file['product'] = product.cells[1].children[0].value;
				file['quantity'] = product.cells[2].children[0].value;

				if (components.length > 0) {
					file['components'] = components.map(component => {
						return component.cells[0].innerHTML;
					})
				}
				return file;
			});

			lot['products'] = files;

			let docRows = document.querySelectorAll('tr');
			Array.from(docRows).map(row => row.hidden = false);

			while (componentsTable.rows.length > 1) {
				componentsTable.deleteRow(-1);
			}
			while (productsTable.rows.length > 1) {
				productsTable.deleteRow(-1);
			}

			storage.addFile(JSON.stringify(lot), this.onLotUpload);
		}
	},

	checkQueue: function(promiQueue) {
		if (promiQueue.getQueueLength() === 0
			&& promiQueue.getPendingLength() === 0)
		{
			this.safe = true;
			let x = document.getElementById("snackbar");
			x.className = x.className.replace("show", "");
		}
	}
}

window.addEventListener('load', function() {
	consumer.start();
});
